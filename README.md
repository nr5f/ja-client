# NR5F Jedi Academy Client Setup
1. Install the game through Steam
2. Extract the contents of the file into the Jedi Academy folder. (For Steam users, this will be in <Steam Folder>/steamapps/common/Jedi Academy/) This will overwrite some files in the GameData folder. That's expected.
3. Run GameData/openjk.x86.exe to launch the game in multiplayer mode
4. Go to Setup > Video, and set the Video Quality to High Quality pre-sets
5. Go to Setup > More Video, and configure the following:
    1. Brightness: set as you desire
    2. Dynamic Lights: On
    3. Dynamic Glow: On
    4. Light Flares: On
    5. Wall Marks: On
    6. Anisotropic Filter: set to maximum
6. Apply the changes and exit the game
7. Edit the following settings to autoexec.cfg as desired:
    1. Add `seta r_customwidth "X"` where X is width of your desired resolution (e.g. `seta r_customwidth "1920"`).
    2. Add `seta r_customheight "Y"` where Y is height of your desired resolution (e.g. `seta r_customheight "1080"`).
    3. Add `seta cg_fov "Z"` to set the FOV. For 16:9 screens use a value of 96.4, for 16:10 screens use a value of 90, for Ultra/Super-Wide (21:9/32:9) screens use a vlaue of 112.7, for Eyefinity/Surround screens use a value of 146.8
    4. Add `seta r_mode "-1"` to make the game use custom resolutions.
    5. Add `seta com_maxfps "120"` to FPS to 120fps
    6. Add `seta r_displayRefresh "120"` to set refresh rate to 120Hz
    7. Add `seta s_kHz "44"` to set sounds to the highest quality
    8. Add `seta cg_dismember "2"` to enable dismemberment
    9. Add `seta broadsword "1"` to enable ragdoll corpses
8. Save and close the autoexec.cfg file
9. Enjoy
